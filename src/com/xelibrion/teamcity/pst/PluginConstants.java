package com.xelibrion.teamcity.pst;

/**
 * Created by Dmitry on 15/03/14.
 */
public final class PluginConstants {
    public static final String PROP_TRIGGER_BUILD_WITH_PENDING_CHANGES_ONLY_PARAM = "triggerBuildWithPendingChangesOnly";
    public static final String PROP_ENFORCE_CLEAN_CHECKOUT_PARAM = "enforceCleanCheckout";
    public static final String PROP_TRIGGER_BUILD_ON_ALL_COMPATIBLE_AGENTS = "triggerBuildOnAllCompatibleAgents";
    public static final String PROP_TRIGGER_RULES_PARAM = "triggerRules";
    public static final String PROP_BRANCH_FILTER = "branchFilter";
    public static final String PROP_PARAMETER_RULES = "parameterRules";

    public static final String TRIGGER_NAME = "ParameterizedScheduleTrigger";
    public static final String TRIGGER_DISPLAY_NAME = "Parameterized schedule trigger";
}

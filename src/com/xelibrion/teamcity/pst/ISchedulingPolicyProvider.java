package com.xelibrion.teamcity.pst;

import jetbrains.buildServer.buildTriggers.BuildTriggerDescriptor;
import jetbrains.buildServer.buildTriggers.scheduler.SchedulingPolicy;

/**
 * Created by Dmitry on 15/03/14.
 */
public interface ISchedulingPolicyProvider {
    SchedulingPolicy getSchedulingPolicy(BuildTriggerDescriptor triggerDescriptor);
}

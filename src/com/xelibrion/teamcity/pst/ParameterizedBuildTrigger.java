package com.xelibrion.teamcity.pst;

import com.xelibrion.teamcity.pst.customization.*;
import jetbrains.buildServer.buildTriggers.BuildTriggerDescriptor;
import jetbrains.buildServer.buildTriggers.BuildTriggerException;
import jetbrains.buildServer.buildTriggers.PolledBuildTrigger;
import jetbrains.buildServer.buildTriggers.PolledTriggerContext;
import jetbrains.buildServer.buildTriggers.scheduler.SchedulingPolicy;
import jetbrains.buildServer.buildTriggers.vcs.VcsBuildTriggerService;
import jetbrains.buildServer.serverSide.*;
import jetbrains.buildServer.util.TimeService;
import jetbrains.buildServer.vcs.BranchFilter;
import jetbrains.buildServer.vcs.SVcsModification;
import jetbrains.buildServer.vcs.SelectPrevBuildPolicy;
import jetbrains.buildServer.vcs.spec.BranchSpecs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ParameterizedBuildTrigger extends PolledBuildTrigger {

    private final SchedulingPolicyProvider schedulingPolicyProvider;
    private long myServerStartupTime;
    private TimeService myTimeService;
    private BranchSpecs branchSpecs;
    private BuildCustomizerFactory buildCustomizerFactory;
    private BatchTrigger batchTrigger;

    ParameterizedBuildTrigger(SchedulingPolicyProvider schedulingPolicyProvider, long myServerStartupTime, TimeService myTimeService, BranchSpecs branchSpecs, BuildCustomizerFactory buildCustomizerFactory, BatchTrigger batchTrigger) {
        this.schedulingPolicyProvider = schedulingPolicyProvider;
        this.myServerStartupTime = myServerStartupTime;
        this.myTimeService = myTimeService;
        this.branchSpecs = branchSpecs;
        this.buildCustomizerFactory = buildCustomizerFactory;
        this.batchTrigger = batchTrigger;
    }


    @Override
    public void triggerBuild(PolledTriggerContext polledTriggerContext) throws BuildTriggerException {

        BuildTypeEx buildType = (BuildTypeEx) polledTriggerContext.getBuildType();
        BuildTriggerDescriptor triggerDescriptor = polledTriggerContext.getTriggerDescriptor();
        Map<String, String> properties = triggerDescriptor.getProperties();
        SchedulingPolicy schedulingPolicy = schedulingPolicyProvider.getSchedulingPolicy(triggerDescriptor);

        Date previousCallTime = polledTriggerContext.getPreviousCallTime();
        if (previousCallTime == null || previousCallTime.getTime() < myServerStartupTime) {
            return;
        }

        long scheduledTime = schedulingPolicy.getScheduledTime(previousCallTime.getTime());
        if (scheduledTime <= 0L || myTimeService.now() < scheduledTime) {
            return;
        }

        kickOffBuild(polledTriggerContext, buildType, properties);
    }

    public void kickOffBuild(PolledTriggerContext polledTriggerContext, BuildTypeEx buildType, Map<String, String> properties) {

        String branchFilter = properties.get(PluginConstants.PROP_BRANCH_FILTER);

        List<BranchEx> branches = buildType.getBranches(BranchesPolicy.VCS_BRANCHES, false);

        if (branchFilter != null) {
            BranchFilter filter = branchSpecs.createFilter(branchFilter);

            for (BranchEx branch : branches) {
                if (filter.accept(branch)) {
                    runCustomizedBuild(polledTriggerContext, branch);
                }
            }
        } else {
            for (BranchEx branch : branches) {
                if (branch.isDefaultBranch()) {
                    runCustomizedBuild(polledTriggerContext, branch);
                }
            }
        }
    }

    public void runCustomizedBuild(PolledTriggerContext polledTriggerContext, BranchEx branch) {
        BuildTypeEx buildType = (BuildTypeEx) polledTriggerContext.getBuildType();
        BuildTriggerDescriptor triggerDescriptor = polledTriggerContext.getTriggerDescriptor();
        Map<String, String> properties = triggerDescriptor.getProperties();

        //check pending changes
        if (triggerOnlyIfPendingChanges(properties) && thereIsNoPendingChanges(buildType, branch, properties)) {
            return;
        }

        BuildCustomizer buildCustomizer = buildCustomizerFactory.createBuildCustomizer(buildType, null);

        new NonDefaultBranchPolicy(buildCustomizer, branch).tryToApply();
        new ParametersCustomizationPolicy(buildCustomizer, properties).tryToApply();
        new EnforceCleanCheckoutPolicy(buildCustomizer, properties).tryToApply();
        new DefaultAgentsSelectionPolicy(buildCustomizer, properties, batchTrigger).tryToApply();
        new AllAgentsSelectionPolicy(buildCustomizer, properties, batchTrigger, buildType).tryToApply();
    }


    private boolean thereIsNoPendingChanges(BuildTypeEx buildType, BranchEx branch, Map<String, String> properties) {
        return hasPendingChanges(buildType, branch, properties) == false;
    }

    protected boolean hasPendingChanges(BuildTypeEx buildType, BranchEx branch, Map<String, String> properties) {
        List<ChangeDescriptor> detectedChanges = branch.getDetectedChanges(SelectPrevBuildPolicy.SINCE_LAST_BUILD, null);
        List<SVcsModification> vcsModifications = new ArrayList<SVcsModification>(detectedChanges.size());

        for (ChangeDescriptor detectedChange : detectedChanges) {
            SVcsModification relatedVcsChange = detectedChange.getRelatedVcsChange();

            if (relatedVcsChange != null) {
                vcsModifications.add(relatedVcsChange);
            }
        }
        VcsBuildTriggerService.filterModificationsByTriggerRules(vcsModifications,
                buildType.getDependencyGraph().getNodes(),
                properties.get(PluginConstants.PROP_TRIGGER_RULES_PARAM));

        return vcsModifications.isEmpty() == false;
    }


    private boolean triggerOnlyIfPendingChanges(Map<String, String> props) {
        String property = props.get(PluginConstants.PROP_TRIGGER_BUILD_WITH_PENDING_CHANGES_ONLY_PARAM);
        return BaseCustomizationPolicy.isTrue(property);
    }
}
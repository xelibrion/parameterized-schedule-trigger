package com.xelibrion.teamcity.pst;


import com.xelibrion.teamcity.pst.customization.ParameterRulesParser;
import jetbrains.buildServer.buildTriggers.BuildTriggerDescriptor;
import jetbrains.buildServer.buildTriggers.BuildTriggerService;
import jetbrains.buildServer.buildTriggers.BuildTriggeringPolicy;
import jetbrains.buildServer.buildTriggers.scheduler.SchedulerBuildTriggerService;
import jetbrains.buildServer.serverSide.*;
import jetbrains.buildServer.util.EventDispatcher;
import jetbrains.buildServer.util.SystemTimeService;
import jetbrains.buildServer.util.TimeService;
import jetbrains.buildServer.vcs.spec.BranchSpecs;
import jetbrains.buildServer.web.openapi.PluginDescriptor;

import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;


public class ParameterizedScheduleTriggerService extends BuildTriggerService {


    private final PluginDescriptor pluginDescriptor;
    private final BatchTrigger batchTrigger;
    private final BuildCustomizerFactory buildCustomizerFactory;
    private final TimeService myTimeService;
    private final BranchSpecs branchSpecs;
    private long myServerStartupTime = 0;


    private SchedulerBuildTriggerService delegate;

    public ParameterizedScheduleTriggerService(PluginDescriptor pluginDescriptor,
                                               EventDispatcher<BuildServerListener> eventDispatcher,
                                               BatchTrigger batchTrigger,
                                               BuildCustomizerFactory buildCustomizerFactory,
                                               BranchSpecs branchSpec) {
        this.pluginDescriptor = pluginDescriptor;
        this.batchTrigger = batchTrigger;
        this.buildCustomizerFactory = buildCustomizerFactory;
        this.branchSpecs = branchSpec;
        this.myTimeService = SystemTimeService.getInstance();
        eventDispatcher.addListener(new BuildServerAdapter() {
            @Override
            public void serverStartup() {
                myServerStartupTime = myTimeService.now();
            }
        });
        this.delegate = new SchedulerBuildTriggerService(eventDispatcher, branchSpecs, batchTrigger, buildCustomizerFactory);
    }

    @Override
    public String getName() {
        return PluginConstants.TRIGGER_NAME;
    }

    @Override
    public String getDisplayName() {
        return PluginConstants.TRIGGER_DISPLAY_NAME;
    }


    @Override
    public String getEditParametersUrl() {
        return pluginDescriptor.getPluginResourcesPath("editView.jsp");
    }

    @Override
    //TODO: tests - need to figure out how to mock SchedulerBuildTriggerService
    public String describeTrigger(BuildTriggerDescriptor buildTriggerDescriptor) {
        final StringBuilder builder = new StringBuilder();

        builder.append(delegate.describeTrigger(buildTriggerDescriptor));

        Map<String, String> properties = buildTriggerDescriptor.getProperties();
        Hashtable<String, String> parameters = ParameterRulesParser.process(properties);

        if (parameters.isEmpty() == false) {
            builder.append("\nParameters:");
            for (Map.Entry<String, String> pair : parameters.entrySet()) {
                builder.append(String.format("%n%s: %s", pair.getKey(), pair.getValue()));
            }
        }

        return builder.toString();
    }

    @Override
    public BuildTriggeringPolicy getBuildTriggeringPolicy() {

        return new ParameterizedBuildTrigger(new SchedulingPolicyProvider(),
                myServerStartupTime,
                myTimeService,
                branchSpecs,
                buildCustomizerFactory,
                batchTrigger
        );
    }

    @Override
    public PropertiesProcessor getTriggerPropertiesProcessor() {
        return new PropertiesProcessor() {
            @Override
            public Collection<InvalidProperty> process(Map<String, String> properties) {
                Collection<InvalidProperty> invalid = delegate.getTriggerPropertiesProcessor().process(properties);

                try {
                    ParameterRulesParser.process(properties);
                } catch (InvalidParameterException ex) {
                    invalid.add(new InvalidProperty(PluginConstants.PROP_PARAMETER_RULES, ex.getMessage()));
                }
                return invalid;
            }
        };
    }

    @Override
    public Map<String, String> getDefaultTriggerProperties() {
        return delegate.getDefaultTriggerProperties();
    }

    @Override
    public boolean isMultipleTriggersPerBuildTypeAllowed() {
        return true;
    }
}
package com.xelibrion.teamcity.pst;

import jetbrains.buildServer.buildTriggers.BuildTriggerDescriptor;
import jetbrains.buildServer.buildTriggers.scheduler.SchedulingPolicy;
import jetbrains.buildServer.buildTriggers.scheduler.SchedulingPolicyFactory;

public class SchedulingPolicyProvider implements ISchedulingPolicyProvider {
    @Override
    public SchedulingPolicy getSchedulingPolicy(BuildTriggerDescriptor triggerDescriptor) {
        return SchedulingPolicyFactory.createSchedulingPolicyOrThrowError(triggerDescriptor);
    }
}
package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;

import java.util.Map;

/**
 * Created by Dmitry on 15/03/14.
 */
public abstract class AgentSelectionPolicy extends BaseCustomizationPolicy {
    protected boolean isTriggerOnAllCompatibleAgents(Map<String, String> properties) {
        String property = properties.get(PluginConstants.PROP_TRIGGER_BUILD_ON_ALL_COMPATIBLE_AGENTS);
        return isTrue(property);
    }
}

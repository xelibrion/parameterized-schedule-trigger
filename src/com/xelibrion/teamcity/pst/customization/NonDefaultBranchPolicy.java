package com.xelibrion.teamcity.pst.customization;

import jetbrains.buildServer.serverSide.Branch;
import jetbrains.buildServer.serverSide.BuildCustomizer;

/**
 * Created by Dmitry on 15/03/14.
 */
public class NonDefaultBranchPolicy extends BaseCustomizationPolicy {
    private BuildCustomizer buildCustomizer;
    private Branch branch;

    public NonDefaultBranchPolicy(BuildCustomizer buildCustomizer, Branch branch) {
        this.buildCustomizer = buildCustomizer;
        this.branch = branch;
    }

    @Override
    protected void apply() {
        buildCustomizer.setDesiredBranchName(branch.getName());
    }

    @Override
    public boolean isApplicable() {
        return branch.isDefaultBranch() == false;
    }
}

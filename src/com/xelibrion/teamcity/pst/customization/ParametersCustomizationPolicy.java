package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;
import jetbrains.buildServer.serverSide.BuildCustomizer;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Dmitry on 15/03/14.
 */
public class ParametersCustomizationPolicy extends BaseCustomizationPolicy {
    private final Map<String, String> properties;
    private BuildCustomizer buildCustomizer;

    public ParametersCustomizationPolicy(BuildCustomizer buildCustomizer, Map<String, String> properties) {
        this.buildCustomizer = buildCustomizer;
        this.properties = properties;
    }

    @Override
    protected void apply() {
        Hashtable<String, String> customParams = ParameterRulesParser.process(properties);

        buildCustomizer.setParameters(customParams);
    }

    @Override
    public boolean isApplicable() {
        return properties.get(PluginConstants.PROP_PARAMETER_RULES) != null;
    }
}

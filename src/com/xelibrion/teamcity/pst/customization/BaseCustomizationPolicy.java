package com.xelibrion.teamcity.pst.customization;


/**
 * Created by Dmitry on 15/03/14.
 */
public abstract class BaseCustomizationPolicy implements ICustomizationPolicy {
    public static boolean isTrue(String property) {
        return Boolean.parseBoolean(property) || "yes".equalsIgnoreCase(property);
    }

    protected abstract void apply();

    @Override
    public final void tryToApply() {
        if (isApplicable())
            apply();
    }
}


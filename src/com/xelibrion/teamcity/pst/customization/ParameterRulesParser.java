package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;

import java.security.InvalidParameterException;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Dmitry on 16/03/14.
 */
public class ParameterRulesParser {
    public static Hashtable<String, String> process(Map<String, String> properties) {
        String rulesAsAString = properties.get(PluginConstants.PROP_PARAMETER_RULES);

        Hashtable<String, String> customParams = new Hashtable<String, String>();

        if (rulesAsAString == null) {
            return customParams;
        }

        String[] rules = rulesAsAString.split("\n");

        for (String rule : rules) {

            String[] keyPair = rule.trim().split(":|=");

            if (keyPair.length != 2) {
                throw new InvalidParameterException("Could not find both parts of key-value pair. Please make sure you are using correct delimiter (colon or equals sign)");
            }

            customParams.put(keyPair[0].trim(), keyPair[1].trim());
        }
        return customParams;
    }
}

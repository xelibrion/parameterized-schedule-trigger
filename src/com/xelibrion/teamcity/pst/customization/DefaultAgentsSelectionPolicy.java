package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;
import jetbrains.buildServer.serverSide.BatchTrigger;
import jetbrains.buildServer.serverSide.BuildCustomizer;
import jetbrains.buildServer.serverSide.TriggerTask;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Dmitry on 15/03/14.
 */
public class DefaultAgentsSelectionPolicy extends AgentSelectionPolicy {
    private BatchTrigger batchTrigger;
    private BuildCustomizer buildCustomizer;
    private Map<String, String> properties;

    public DefaultAgentsSelectionPolicy(BuildCustomizer buildCustomizer, Map<String, String> properties, BatchTrigger batchTrigger) {
        this.batchTrigger = batchTrigger;
        this.buildCustomizer = buildCustomizer;
        this.properties = properties;
    }

    @Override
    protected void apply() {
        List<TriggerTask> tasks = new LinkedList<TriggerTask>();
        tasks.add(batchTrigger.newTriggerTask(buildCustomizer.createPromotion()));
        batchTrigger.processTasks(tasks, PluginConstants.TRIGGER_DISPLAY_NAME);
    }

    @Override
    public boolean isApplicable() {
        return isTriggerOnAllCompatibleAgents(properties) == false;
    }
}

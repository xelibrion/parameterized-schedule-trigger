package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;
import jetbrains.buildServer.serverSide.BuildCustomizer;

import java.util.Map;

/**
 * Created by Dmitry on 15/03/14.
 */
public class EnforceCleanCheckoutPolicy extends BaseCustomizationPolicy {
    private final Map<String, String> properties;
    private final BuildCustomizer buildCustomizer;

    public EnforceCleanCheckoutPolicy(BuildCustomizer buildCustomizer, Map<String, String> properties) {
        this.properties = properties;
        this.buildCustomizer = buildCustomizer;
    }

    @Override
    public boolean isApplicable() {
        return isTrue(properties.get(PluginConstants.PROP_ENFORCE_CLEAN_CHECKOUT_PARAM));
    }

    @Override
    protected void apply() {
        buildCustomizer.setCleanSources(true);
    }
}

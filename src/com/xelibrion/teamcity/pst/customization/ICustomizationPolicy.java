package com.xelibrion.teamcity.pst.customization;

public interface ICustomizationPolicy {
    boolean isApplicable();

    void tryToApply();
}
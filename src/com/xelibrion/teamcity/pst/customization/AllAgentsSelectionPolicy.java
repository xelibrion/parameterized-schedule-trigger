package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;
import jetbrains.buildServer.BuildAgent;
import jetbrains.buildServer.serverSide.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Dmitry on 15/03/14.
 */
public class AllAgentsSelectionPolicy extends AgentSelectionPolicy {
    private Map<String, String> properties;
    private SBuildType buildType;
    private BuildCustomizer buildCustomizer;
    private BatchTrigger batchTrigger;

    public AllAgentsSelectionPolicy(BuildCustomizer buildCustomizer, Map<String, String> properties, BatchTrigger batchTrigger, SBuildType buildType) {
        this.properties = properties;
        this.buildType = buildType;
        this.buildCustomizer = buildCustomizer;
        this.batchTrigger = batchTrigger;
    }

    @Override
    protected void apply() {
        for (BuildAgent buildAgent : buildType.getCanRunAndCompatibleAgents(false)) {
            List<TriggerTask> tasks = new LinkedList<TriggerTask>();
            TriggerTask task = batchTrigger.newTriggerTask(buildCustomizer.createPromotion());
            task.setRunOnAgent((SBuildAgent) buildAgent);
            tasks.add(task);
            batchTrigger.processTasks(tasks, PluginConstants.TRIGGER_DISPLAY_NAME);
        }
    }

    @Override
    public boolean isApplicable() {
        return isTriggerOnAllCompatibleAgents(properties);
    }
}

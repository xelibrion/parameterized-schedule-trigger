<%@ page import="jetbrains.buildServer.buildTriggers.scheduler.CronFieldInfo" %>
<%@ page import="java.util.TimeZone" %>
<%@ page import="jetbrains.buildServer.serverSide.Branch" %>
<%@ include file="/include.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="util" uri="/WEB-INF/functions/util"%>
<jsp:useBean id="propertiesBean" type="jetbrains.buildServer.controllers.BasePropertiesBean" scope="request"/>
<tr>
  <td colspan="2"><em>Build scheduler adds a build to the queue at specified time.</em></td>
</tr>
<tr>
  <td><label for="schedulingPolicy">Trigger build:</label></td>
  <td>
    <c:set var="onchange">{
        var idx = $('schedulingPolicy').selectedIndex;
        $('weeklyPolicy').style.display = idx == 1 ? '' : 'none';
        $('cronPolicy').style.display = idx == 2 ? '' : 'none';
        $('dailyPolicy').style.display = idx == 0 || idx == 1 ? '' : 'none';
        if (idx == 2) {
          $('cronHelp').show();
        } else {
          $('cronHelp').hide();
        }
        BS.MultilineProperties.updateVisible();
    }</c:set>
    <props:selectProperty name="schedulingPolicy" onchange="${onchange}">
      <props:option value="daily">daily</props:option>
      <props:option value="weekly">weekly</props:option>
      <props:option value="cron">advanced (cron expression)</props:option>
    </props:selectProperty> <bs:help id="cronHelp" file="Configuring+Schedule+Triggers" style="${propertiesBean.properties['schedulingPolicy'] == 'cron' ? '' : 'display: none;'}"/>
  </td>
</tr>
<tr id="weeklyPolicy" style="${propertiesBean.properties['schedulingPolicy'] == 'weekly' ? '' : 'display: none;'}">
  <td>
    <label for="dayOfWeek">Day of the week:</label>
  </td>
  <td>
    <props:selectProperty name="dayOfWeek">
      <props:option value="Sunday">Sunday</props:option>
      <props:option value="Monday">Monday</props:option>
      <props:option value="Tuesday">Tuesday</props:option>
      <props:option value="Wednesday">Wednesday</props:option>
      <props:option value="Thursday">Thursday</props:option>
      <props:option value="Friday">Friday</props:option>
      <props:option value="Saturday">Saturday</props:option>
    </props:selectProperty>
  </td>
</tr>
<tr id="dailyPolicy" style="${propertiesBean.properties['schedulingPolicy'] == 'cron' ? 'display: none;' : ''}">
  <td>
    <label for="hour">Time (HH:mm):</label>
  </td>
  <td>
    <props:selectProperty name="hour">
      <c:forEach begin="0" end="23" step="1" varStatus="pos">
        <props:option value="${pos.index}"><c:if test="${pos.index < 10}">0</c:if>${pos.index}</props:option>
      </c:forEach>
    </props:selectProperty>
    <props:selectProperty name="minute">
      <c:forEach begin="0" end="59" step="5" varStatus="pos">
        <props:option value="${pos.index}"><c:if test="${pos.index < 10}">0</c:if>${pos.index}</props:option>
      </c:forEach>
    </props:selectProperty>
  </td>
</tr>
<tr id="cronPolicy" style="${propertiesBean.properties['schedulingPolicy'] == 'cron' ? '' : 'display: none;'}">
  <td colspan="2" class="noBorder" style="padding: 0;">
    <table style="width: 100%;">
    <c:set var="cronFields" value="<%=CronFieldInfo.values()%>"/>
    <c:forEach items="${cronFields}" var="field">
    <c:set var="fieldElement" value="cronExpression_${field.key}"/>
    <tr>
      <td style="width: 30%;">
        <label for="${fieldElement}"><c:out value="${field.caption}"/> <c:out value="${field.descr}"/>:</label>
      </td>
      <td>
        <props:textProperty name="${fieldElement}" maxlength="100" style="width: 8em;" className="disableBuildTypeParams"/>
        <span class="error" id="error_${fieldElement}"></span>
      </td>
    </tr>
    </c:forEach>
    </table>
    <span class="error" id="error_cronExpressionError"></span>
  </td>
</tr>
<tr class="noBorder advancedSetting" id="timezoneProps">
  <th class="noBorder" style="font-weight: normal">
    <label for="timezone">Timezone:</label>
  </th>
  <td class="noBorder">
    <c:set var="serverTimeZone" value="<%=TimeZone.getDefault()%>"/>
    <props:selectProperty name="timezone" enableFilter="true">
      <props:option value="SERVER">Server Time Zone - ${util:formatTimeZone(serverTimeZone)}</props:option>
      <c:forEach items="${util:timeZones()}" var="zone">
        <props:option value="${zone.ID}">${util:formatTimeZone(zone)}</props:option>
      </c:forEach>
    </props:selectProperty>
  </td>
</tr>
<tr>
  <th colspan="2" style="font-weight: normal">
    <props:checkboxProperty name="triggerBuildWithPendingChangesOnly"/>
    <label for="triggerBuildWithPendingChangesOnly">Trigger build only if there are pending changes</label>
  </th>
</tr>
<tr class="advancedSetting">
  <th colspan="2" style="font-weight: normal">
    <props:checkboxProperty name="triggerBuildOnAllCompatibleAgents"/>
    <label for="triggerBuildOnAllCompatibleAgents">Trigger build on all enabled and compatible agents</label>
  </th>
</tr>
<tr class="advancedSetting">
  <th colspan="2" style="font-weight: normal">
    <props:checkboxProperty name="enforceCleanCheckout"/>
    <label for="enforceCleanCheckout">Clean all files in checkout directory before build</label>
  </th>
</tr>
<l:settingsGroup title="Parameter Rules"/>
<tr>
  <th style="vertical-align: top;">
    <label for="parameterRules" class="rightLabel">Parameter rules:</label>
  </th>
  <td style="vertical-align: top;">
    <props:multilineProperty name="parameterRules" linkTitle="Edit Parameter Rules" cols="35" rows="3"/>
    <span class="error" id="error_parameterRules"></span>
    <span class="smallNote">Newline-delimited set of rules in the form of key:value or key=value</span>
    <script type="text/javascript">
      BS.MultilineProperties.updateVisible();
    </script>
  </td>
</tr>
<jsp:include page="/admin/triggers/triggerRulesAndBranchFilter.jsp">
  <jsp:param name="defaultBranchFilter" value='<%="+:" + Branch.DEFAULT_BRANCH_NAME%>'/>
</jsp:include>

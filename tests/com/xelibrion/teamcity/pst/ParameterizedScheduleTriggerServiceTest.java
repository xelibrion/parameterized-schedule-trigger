package com.xelibrion.teamcity.pst;

import jetbrains.buildServer.buildTriggers.BuildTriggerDescriptor;
import jetbrains.buildServer.buildTriggers.BuildTriggerDescriptorImpl;
import jetbrains.buildServer.serverSide.BatchTrigger;
import jetbrains.buildServer.serverSide.BuildCustomizerFactory;
import jetbrains.buildServer.serverSide.BuildServerListener;
import jetbrains.buildServer.serverSide.BuildServerListenerEventDispatcher;
import jetbrains.buildServer.util.EventDispatcher;
import jetbrains.buildServer.vcs.spec.BranchSpecs;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by Dmitry on 16/03/14.
 */
public class ParameterizedScheduleTriggerServiceTest {


    @Test
    @Ignore
    public void testDescribeTrigger() throws Exception {

        PluginDescriptor pluginDescriptor = Mockito.mock(PluginDescriptor.class);
        EventDispatcher<BuildServerListener> eventDispatcher = Mockito.mock(EventDispatcher.class);
        BatchTrigger batchTrigger = Mockito.mock(BatchTrigger.class);
        BuildCustomizerFactory buildCustomizerFactory = Mockito.mock(BuildCustomizerFactory.class);
        BranchSpecs branchSpecs = Mockito.mock(BranchSpecs.class);
        BuildTriggerDescriptor buildTriggerDescriptor = Mockito.mock(BuildTriggerDescriptor.class);

        ParameterizedScheduleTriggerService triggerService = new ParameterizedScheduleTriggerService(pluginDescriptor, eventDispatcher, batchTrigger, buildCustomizerFactory, branchSpecs);

        triggerService.describeTrigger(buildTriggerDescriptor);
    }
}

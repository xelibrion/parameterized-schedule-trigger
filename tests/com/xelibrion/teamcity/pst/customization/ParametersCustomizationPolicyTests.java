package com.xelibrion.teamcity.pst.customization;

import com.xelibrion.teamcity.pst.PluginConstants;
import jetbrains.buildServer.serverSide.BuildCustomizer;
import junit.framework.Assert;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.security.InvalidParameterException;
import java.util.Hashtable;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Dmitry on 15/03/14.
 */
public class ParametersCustomizationPolicyTests {

    @Test
    public void applyCore_whenSingleParameterSpecified_callsSetParameterOnce() throws Exception {

        BuildCustomizer buildCustomizer = Mockito.mock(BuildCustomizer.class);

        Hashtable<String, String> properties = new Hashtable<String, String>();
        properties.put(PluginConstants.PROP_PARAMETER_RULES, "environment: a");

        Hashtable<String, String> customParams = new Hashtable<String, String>();
        customParams.put("environment", "a");

        ParametersCustomizationPolicy policy = new ParametersCustomizationPolicy(buildCustomizer, properties);
        policy.apply();

        verify(buildCustomizer, times(1)).setParameters(Matchers.eq(customParams));
    }

    @Test
    public void applyCore_whenMultipleParametersSpecified_callsSetParameterOnce() throws Exception {

        BuildCustomizer buildCustomizer = Mockito.mock(BuildCustomizer.class);

        Hashtable<String, String> properties = new Hashtable<String, String>();
        String value = String.format("environment: a%ntestSuite: reliable");
        properties.put(PluginConstants.PROP_PARAMETER_RULES, value);

        Hashtable<String, String> customParams = new Hashtable<String, String>();
        customParams.put("environment", "a");
        customParams.put("testSuite", "reliable");

        ParametersCustomizationPolicy policy = new ParametersCustomizationPolicy(buildCustomizer, properties);
        policy.apply();

        verify(buildCustomizer, times(1)).setParameters(Matchers.eq(customParams));
    }

    @Test
    public void applyCore_whenMultipleParametersSpecifiedUsingDifferentSeparators_doesNotThrow() throws Exception {

        BuildCustomizer buildCustomizer = Mockito.mock(BuildCustomizer.class);

        Hashtable<String, String> properties = new Hashtable<String, String>();
        String value = String.format("environment: a%ntestSuite= reliable");
        properties.put(PluginConstants.PROP_PARAMETER_RULES, value);

        Hashtable<String, String> customParams = new Hashtable<String, String>();
        customParams.put("environment", "a");
        customParams.put("testSuite", "reliable");

        ParametersCustomizationPolicy policy = new ParametersCustomizationPolicy(buildCustomizer, properties);
        policy.apply();

        verify(buildCustomizer, times(1)).setParameters(Matchers.eq(customParams));
    }

    @Test(expected = InvalidParameterException.class)
    public void applyCore_whenParameterRulesAreIncorrect_throws() throws Exception {

        BuildCustomizer buildCustomizer = Mockito.mock(BuildCustomizer.class);

        Hashtable<String, String> properties = new Hashtable<String, String>();
        properties.put(PluginConstants.PROP_PARAMETER_RULES, "environment");

        ParametersCustomizationPolicy policy = new ParametersCustomizationPolicy(buildCustomizer, properties);
        policy.apply();
    }

    @Test
    public void isApplicable_whenNoRulesSpecified_returnsFalse() throws Exception {
        BuildCustomizer buildCustomizer = Mockito.mock(BuildCustomizer.class);

        Hashtable<String, String> properties = new Hashtable<String, String>();
        ParametersCustomizationPolicy policy = new ParametersCustomizationPolicy(buildCustomizer, properties);

        Assert.assertFalse(policy.isApplicable());
    }

    @Test
    public void isApplicable_whenParameterRulesSpecified_returnsTrue() throws Exception {
        BuildCustomizer buildCustomizer = Mockito.mock(BuildCustomizer.class);

        Hashtable<String, String> properties = new Hashtable<String, String>();
        properties.put(PluginConstants.PROP_PARAMETER_RULES, "environment: a");
        ParametersCustomizationPolicy policy = new ParametersCustomizationPolicy(buildCustomizer, properties);

        Assert.assertTrue(policy.isApplicable());
    }

    class ApplyCore {

    }
}

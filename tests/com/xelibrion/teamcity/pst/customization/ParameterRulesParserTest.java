package com.xelibrion.teamcity.pst.customization;

import junit.framework.Assert;
import org.junit.Test;

import java.util.Hashtable;

/**
 * Created by Dmitry on 16/03/14.
 */
public class ParameterRulesParserTest {
    @Test
    public void process_whenRulesAreNotSpecified_doesNotThrow() throws Exception {
        Hashtable<String, String> params = ParameterRulesParser.process(new Hashtable<String, String>());

        Assert.assertTrue(params.isEmpty());
    }
}
